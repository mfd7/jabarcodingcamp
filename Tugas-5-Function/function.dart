void main(){
    // Soal No 1
    print(teriak());

    // Soal No 2
    var num1 = 12;
    var num2 = 4;

    var hasilKali = kalikan(num1, num2);
    print(hasilKali);

    // Soal No 3
    var name = "Agus";
    var age = 30;
    var address = "Jln. Malioboro, Yogyakarta";
    var hobby = "Gaming";
    
    var perkenalan = introduce(name, age, address, hobby);
    print(perkenalan);

    // Soal No 4
    var hitungFactorial = factorial(6);
    print(hitungFactorial);
}

// Soal No 1
teriak(){
    return "Halo Sanbers!";
}

// Soal No 2
kalikan(var num1, var num2){
    return num1 * num2;
}

// Soal No 3
introduce(name, age, address, hobby){
    return "Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}";
}

// Soal No 4
factorial(x){
    if(x <= 0){
        return 1;
    }else{
        var hasil = 1;
        for(var i = x; i > 0; i--){
            hasil = hasil * i as int;
        }
        return hasil;
    }
}