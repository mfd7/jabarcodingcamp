import "dart:io";

void main(){
    // Soal No 1
    var isInstall = false;

    print("Apakah anda ingin menginstall dart? (y/t)");
    var inputUser = stdin.readLineSync()?.toLowerCase();
    if(inputUser == "y"){
        isInstall = true;
    }else if(inputUser == "t"){
        isInstall = false;
    }

    isInstall ? print("Anda akan menginstall aplikasi dart") : print("aborted");

    // Soal No 2
    String? nama;
    String? peran;

    print("Nama: ");
    nama = stdin.readLineSync()?.toLowerCase();
    print("Peran: ");
    peran = stdin.readLineSync()?.toLowerCase();

    if(nama != null && peran != null){
        if(nama.isEmpty && peran.isEmpty){
            print("Nama harus diisi!");
        }else if(nama.isEmpty && !peran.isEmpty){
            print("Nama harus diisi!");
        }else if(!nama.isEmpty && peran.isEmpty){
            print("Halo ${nama}, pilih peranmu untuk memulai game!");
        }else if(!nama.isEmpty && !peran.isEmpty){
            if(peran == "penyihir"){
                print("Selamat datang di dunia werewolf, ${nama}");
                print("Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!");
            }else if(peran == "guard"){
                print("Selamat datang di dunia werewolf, ${nama}");
                print("Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf!");
            }else if(peran == "werewolf"){
                print("Selamat datang di dunia werewolf, ${nama}");
                print("Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam!");
            }
        }
    }

    // Soal No 3
    String? inputHari;

    print("Masukkan Hari: ");
    inputHari = stdin.readLineSync();

    if(inputHari != null){
        switch(inputHari){
            case "senin": {print("Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja."); break;}
            case "selasa": {print("Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati."); break;}
            case "rabu": {print("Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri."); break;}
            case "kamis": {print("Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain."); break;}
            case "jumat": {print("Hidup tak selamanya tentang pacar."); break;}
            case "sabtu": {print("Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan."); break;}
            case "minggu": {print("Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani."); break;}
            default: {print("Tidak terjadi apa-apa");}
        }
    }

    // Soal No 4
    var hari = 4;
    var bulan = 8;
    var tahun = 1999;
    var bulanHuruf;

    if(hari >= 1 && hari <= 31 && bulan >= 1 && bulan <= 12 && tahun >= 1900 && tahun <= 2200){
        switch(bulan){
            case 1: {bulanHuruf = "Januari"; break;}
            case 2: {bulanHuruf = "Februari"; break;}
            case 3: {bulanHuruf = "Maret"; break;}
            case 4: {bulanHuruf = "April"; break;}
            case 5: {bulanHuruf = "Mei"; break;}
            case 6: {bulanHuruf = "Juni"; break;}
            case 7: {bulanHuruf = "Juli"; break;}
            case 8: {bulanHuruf = "Agustus"; break;}
            case 9: {bulanHuruf = "September"; break;}
            case 10: {bulanHuruf = "Oktober"; break;}
            case 11: {bulanHuruf = "November"; break;}
            case 12: {bulanHuruf = "Desember"; break;}
            default: {print("Tidak terjadi apa-apa");}
        }
        print("${hari} ${bulanHuruf} ${tahun}");
    }
    
}