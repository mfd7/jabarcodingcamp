Future<void> main(List<String> args) async {
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  return Future.delayed(Duration(seconds: 5), () => "pernahkah kau merasa");
}

Future<String> line2() async {
  return Future.delayed(
      Duration(seconds: 3), () => "pernahkan kau merasa.....");
}

Future<String> line3() async {
  return Future.delayed(Duration(seconds: 2), () => "pernahkah kau merasa");
}

Future<String> line4() async {
  return Future.delayed(Duration(seconds: 1),
      () => "Hatimu hampa, pernahkah kau merasa hati mu kosong....");
}
