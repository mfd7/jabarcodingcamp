import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas13/DrawerScreen.dart';
import 'package:hexcolor/hexcolor.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HOME"),
        backgroundColor: HexColor('#8FC928'),
        centerTitle: true,
      ),
      drawer: DrawerScreen(),
      body: ListView(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 50, right: 20, bottom: 5, left: 20),
            child: Text(
              'Welcome,',
              style: TextStyle(
                fontSize: 60,
                foreground: Paint()
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = 2
                  ..color = HexColor('#8FC928'),
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 0, right: 20, bottom: 5, left: 20),
            child: Text(
              'Dzaky',
              style: TextStyle(
                fontSize: 60,
                foreground: Paint()
                  ..style = PaintingStyle.stroke
                  ..strokeWidth = 2
                  ..color = Colors.blue,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
