import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:jccappflutter/Tugas/Tugas13/AboutScreen.dart';
import 'package:jccappflutter/Tugas/Tugas13/LoginScreen.dart';
import 'package:jccappflutter/Tugas/Tugas13/SkillsScreen.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text("Mohammad Faishal Dzaky"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage("assets/img/foto.jpg"),
                  ),
                  accountEmail: Text("mfdzaky7@gmail.com"),
                  decoration: BoxDecoration(color: HexColor('#8FC928')),
                ),
                DrawerListTitle(
                  iconData: Icons.code_sharp,
                  title: "Skills",
                  onTilePressed: () {
                    var route = new MaterialPageRoute(
                        builder: (BuildContext context) => SkillsScreen());
                    Navigator.of(context).push(route);
                  },
                ),
                DrawerListTitle(
                  iconData: Icons.info,
                  title: "About Me",
                  onTilePressed: () {
                    var route = new MaterialPageRoute(
                        builder: (BuildContext context) => AboutScreen());
                    Navigator.of(context).push(route);
                  },
                ),
              ],
            ),
          ),
          Container(
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      Divider(),
                      DrawerListTitle(
                        iconData: Icons.power_settings_new,
                        title: "Log Out",
                        onTilePressed: () {
                          var route = new MaterialPageRoute(
                              builder: (BuildContext context) => LoginScreen());
                          Navigator.of(context).push(route);
                        },
                      ),
                    ],
                  ))))
        ],
      ),
    );
  }
}

class DrawerListTitle extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTitle(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
