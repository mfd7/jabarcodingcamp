import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SkillsScreen extends StatefulWidget {
  const SkillsScreen({Key? key}) : super(key: key);

  @override
  _SkillsScreenState createState() => _SkillsScreenState();
}

class _SkillsScreenState extends State<SkillsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PROGRAMMING SKILLS"),
        backgroundColor: HexColor('#8FC928'),
        centerTitle: true,
      ),
      body: ListView(children: [
        Stack(
          children: <Widget>[
            Image(
              image: AssetImage('assets/img/login.png'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 150),
              child: Center(
                child: CircleAvatar(
                  foregroundColor: Colors.blue,
                  backgroundColor: Colors.white,
                  radius: 80.0,
                  child: ClipOval(
                    child: Image.asset(
                      'assets/img/user.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, right: 8, bottom: 20, left: 8),
          child: Text(
            'User Name',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Programming Languages',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: Image(
              image: AssetImage('assets/img/dart.png'),
              width: 50,
            ),
            title: Text('Dart'),
            subtitle: Text('Skill Level   : Advance\nProficiency : 100%'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: Image(
              image: AssetImage('assets/img/java.png'),
              width: 50,
            ),
            title: Text('Java'),
            subtitle: Text('Skill Level   : Intermediate\nProficiency : 55%'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Frameworks',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: Image(
              image: AssetImage('assets/img/flutter.png'),
              width: 50,
            ),
            title: Text('Flutter'),
            subtitle: Text('Skill Level   : Advance\nProficiency : 90%'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'Technologies',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: Image(
              image: AssetImage('assets/img/android-studio.png'),
              width: 50,
            ),
            title: Text('Android Studio'),
            subtitle: Text('Skill Level   : Advance\nProficiency : 100%'),
          ),
        ),
      ]),
    );
  }
}
