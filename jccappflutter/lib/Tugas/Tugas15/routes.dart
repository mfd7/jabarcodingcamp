import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas15/AboutScreen.dart';
import 'package:jccappflutter/Tugas/Tugas15/HomeScreen.dart';
import 'package:jccappflutter/Tugas/Tugas15/LoginScreen.dart';
import 'package:jccappflutter/Tugas/Tugas15/RegisterScreen.dart';
import 'package:jccappflutter/Tugas/Tugas15/Screen.dart';
import 'package:jccappflutter/Tugas/Tugas15/SkillsScreen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // jika ingin mengirim argument
    // final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case '/about':
        return MaterialPageRoute(builder: (_) => AboutScreen());
      case '/home':
        return MaterialPageRoute(builder: (_) => Home());
      case '/skills':
        return MaterialPageRoute(builder: (_) => SkillsScreen());
      case '/register':
        return MaterialPageRoute(builder: (_) => RegisterScreen());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Error")),
        body: Center(child: Text('Error page')),
      );
    });
  }
}
