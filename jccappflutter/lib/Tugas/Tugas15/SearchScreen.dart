import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:jccappflutter/Tugas/Tugas15/DrawerScreen.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SEARCH"),
        backgroundColor: HexColor('#8FC928'),
        centerTitle: true,
      ),
      drawer: DrawerScreen(),
      body: Center(child: Text('Search Page')),
    );
  }
}
