import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ABOUT ME"),
        backgroundColor: HexColor('#8FC928'),
        centerTitle: true,
      ),
      body: ListView(children: [
        Stack(
          children: <Widget>[
            Image(
              image: AssetImage('assets/img/login.png'),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 150),
              child: Center(
                child: CircleAvatar(
                  foregroundColor: Colors.blue,
                  backgroundColor: Colors.white,
                  radius: 80.0,
                  child: ClipOval(
                    child: Image.asset(
                      'assets/img/foto.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8, right: 8, bottom: 20, left: 8),
          child: Text(
            'Mohammad Faishal Dzaky',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'My Social Accounts',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: FaIcon(
              FontAwesomeIcons.facebook,
              size: 50,
            ),
            title: Text('Mohammad Faishal Dzaky'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: FaIcon(
              FontAwesomeIcons.twitter,
              size: 50,
            ),
            title: Text('mfdzaky7'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: FaIcon(
              FontAwesomeIcons.instagram,
              size: 50,
            ),
            title: Text('mfaishaldzaky'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'My Portofolios',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: FaIcon(
              FontAwesomeIcons.github,
              size: 50,
            ),
            title: Text('mfd7'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: const ListTile(
            leading: FaIcon(
              FontAwesomeIcons.gitlab,
              size: 50,
            ),
            title: Text('mfd7'),
          ),
        ),
      ]),
    );
  }
}
