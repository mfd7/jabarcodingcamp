import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:jccappflutter/Tugas/Tugas15/DrawerScreen.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ACCOUNT"),
        backgroundColor: HexColor('#8FC928'),
        centerTitle: true,
      ),
      drawer: DrawerScreen(),
      body: Center(child: Text('Account Page')),
    );
  }
}
