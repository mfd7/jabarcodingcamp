import 'package:flutter/material.dart';
import 'package:jccappflutter/Quiz3/screen/home/model.dart';

class Homescreen extends StatefulWidget {
  final String username;

  Homescreen({Key? key, required this.username}) : super(key: key);

  @override
  _HomescreenState createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  var total = 0;
  _HomescreenState();
  @override
  void initState() {
    super.initState();
  }

  addToCart(int harga) {
    setState(() {
      total += harga;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40.0),
                      child: Image.network(
                        "https://avatars.githubusercontent.com/u/52710807?v=4",
                        height: 80,
                        width: 80,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(widget.username),
                  ],
                ),
                Row(
                  children: [
                    //## soal 4
                    //Tulis Coding disini
                    Text("Rp${total}"),

                    //sampai disini
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.add_shopping_cart)
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            //#soal 3 silahkan buat coding disini
            //untuk container boleh di pake/dimodifikasi
            Container(
              height: MediaQuery.of(context).size.height / 1.5,
              child: GridView.builder(
                itemBuilder: (ctx, i) {
                  return Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(color: Colors.lightBlue, blurRadius: 0.3)
                    ]),
                    child: Card(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: Image(
                              image: NetworkImage(items[i].profileUrl),
                              width: 120,
                            ),
                          ),
                          Center(child: Container(child: Text(items[i].name))),
                          Center(
                              child: Container(
                                  child: Text(items[i].price.toString()))),
                          ElevatedButton(
                              onPressed: () {
                                addToCart(items[i].price);
                              },
                              child: Text('Beli'))
                        ],
                      ),
                    ),
                  );
                },
                itemCount: items.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10),
              ),
            ),

            //sampai disini soal 3
          ],
        ),
      ),
    );
  }
}

Widget myWidget(BuildContext context) {
  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: 300,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.amber,
            child: Center(child: Text('$index')),
          );
        }),
  );
}
