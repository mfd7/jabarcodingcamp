import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas/Tugas12/Telegram.dart';
import 'package:jccappflutter/Tugas/Tugas14/get_data.dart';

//import 'Tugas/Tugas13/LoginScreen.dart' as Login13;
//import 'Tugas/Tugas15/LoginScreen.dart' as Login15;
//import 'Tugas/Tugas15/routes.dart' as Routes15;
import 'Latihan/Authentication/routes.dart';
import 'Latihan/Authentication/LoginScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
    );
  }
}
