import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hexcolor/hexcolor.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text("Mohammad Faishal Dzaky"),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: AssetImage("assets/img/foto.jpg"),
                  ),
                  accountEmail: Text(auth.currentUser!.email.toString()),
                  decoration: BoxDecoration(color: HexColor('#8FC928')),
                ),
                DrawerListTitle(
                  iconData: Icons.code_sharp,
                  title: "Skills",
                  onTilePressed: () {},
                ),
                DrawerListTitle(
                  iconData: Icons.info,
                  title: "About Me",
                  onTilePressed: () {},
                ),
              ],
            ),
          ),
          Container(
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                      child: Column(
                    children: <Widget>[
                      Divider(),
                      DrawerListTitle(
                        iconData: Icons.power_settings_new,
                        title: "Log Out",
                        onTilePressed: () {
                          _signOut().then((value) =>
                              Navigator.of(context).pushReplacementNamed('/'));
                        },
                      ),
                    ],
                  ))))
        ],
      ),
    );
  }
}

class DrawerListTitle extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;

  const DrawerListTitle(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}
