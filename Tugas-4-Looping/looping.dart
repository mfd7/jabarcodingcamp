import "dart:io";

void main(){
    // Soal No 1
    // LOOPING PERTAMA
    print("LOOPING PERTAMA");
    var index1 = 2;
    while(index1 <= 20){
        if(index1 % 2 == 0){
            print("${index1} - I love coding");
        }
        index1++;
    }

    // LOOPING KEDUA
    print("LOOPING KEDUA");
    var index2 = 20;
    while(index2 > 1){
        if(index2 % 2 == 0){
            print("${index2} - I will become a mobile developer");
        }
        index2--;
    }

    // Soal No 2
    for(var i = 1; i <= 20; i++){
        if(i % 3 != 0 && i % 2 != 0){
            print("${i} - Santai");
        }else if(i % 2 == 0){
            print("${i} - Berkualitas");
        }else if(i % 3 == 0 && i % 2 != 0){
            print("${i} - I Love Coding");
        }
    }

    // Soal No 3
    for(var i = 0; i < 4; i++){
        for(var j = 0; j < 8; j++){
            stdout.write("#");
        }
        print("");
    }

    // Soal No 4
    for(var i = 1; i < 8; i++){
        for(var j = 0; j < i; j++){
            stdout.write("#");
        }
        print("");
    }
}