import 'bangun_datar.dart';
import 'dart:math';

class Segitiga extends BangunDatar{
  late double alas;
  late double tinggi;

  Segitiga(double alas, double tinggi){
    this.alas = alas;
    this.tinggi = tinggi;
  }

  @override
  double hitungLuas() {
    return 0.5 * alas * tinggi;
  }

  @override
  double hitungKeliling() {
    var a = alas;
    var b = tinggi;
    var c = sqrt(pow(a, 2) + pow(b, 2));
    return a + b + c;
  }
}