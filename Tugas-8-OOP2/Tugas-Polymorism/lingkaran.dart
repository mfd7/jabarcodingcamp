import 'bangun_datar.dart';
import 'dart:math';

class Lingkaran extends BangunDatar{
  late double r;

  Lingkaran(double r){
    this.r = r;
  }

  @override
  double hitungLuas() {
    return 3.14 * pow(r, 2);
  }

  @override
  double hitungKeliling() {
    return 2 * 3.14 * r;
  }
}