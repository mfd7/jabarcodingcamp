import 'dart:io';

import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';
import 'lingkaran.dart';
void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  print("Sisi persegi: ");
  Persegi persegi = new Persegi(double.parse(stdin.readLineSync()!));
  print("Alas dan Tinggi Segitiga: ");
  Segitiga segitiga = new Segitiga(double.parse(stdin.readLineSync()!), double.parse(stdin.readLineSync()!));
  print("Jari-jari Lingkaran: ");
  Lingkaran lingkaran = new Lingkaran(double.parse(stdin.readLineSync()!));

  bangunDatar.hitungLuas();
  bangunDatar.hitungKeliling();

  print("Luas Persegi: ${persegi.hitungLuas()}");
  print("Keliling Persegi: ${persegi.hitungKeliling()}");
  print("Luas Segitiga: ${segitiga.hitungLuas()}");
  print("Keliling Segitiga: ${segitiga.hitungKeliling()}");
  print("Luas Lingkaran: ${lingkaran.hitungLuas()}");
  print("Keliling Lingkaran: ${lingkaran.hitungKeliling()}");
}