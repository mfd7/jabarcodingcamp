import 'bangun_datar.dart';

class Persegi extends BangunDatar {
  late double sisi;

  Persegi(double sisi) {
    this.sisi = sisi;
    }

  @override
  double hitungLuas() {
    return sisi * sisi;
  }

  @override
  double hitungKeliling() {
    return 4 * sisi;
  }
}
