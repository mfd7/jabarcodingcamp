import 'dart:io';

import 'human.dart';
import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';

void main(List<String> args) {
  Human human = Human();
  ArmorTitan armorTitan = ArmorTitan();
  AttackTitan attackTitan = AttackTitan();
  BeastTitan beastTitan = BeastTitan();

  print("Power Point Human: ");
  human.powerPoint = int.parse(stdin.readLineSync()!);
  print("Power Point Armor Titan: ");
  armorTitan.powerPoint = int.parse(stdin.readLineSync()!);
  print("Power Point Attack Titan: ");
  attackTitan.powerPoint = int.parse(stdin.readLineSync()!);
  print("Power Point Beast Titan: ");
  beastTitan.powerPoint = int.parse(stdin.readLineSync()!);

  print("Power point human: ${human.powerPoint}");
  print("Power point armor titan: ${armorTitan.powerPoint}");
  print("Power point attack titan: ${attackTitan.powerPoint}");
  print("Power point beast titan: ${beastTitan.powerPoint}");

  print("Sifat dari human: "+ human.killAllTitan());
  print("Sifat dari armor titan: "+ armorTitan.terjang());
  print("Sifat dari attack titan: "+ attackTitan.punch());
  print("Sifat dari beast titan: "+ beastTitan.lempar());
}