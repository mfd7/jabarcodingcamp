import 'dart:io';

void main(){
    // Soal 1
    print("==== Soal Nomor 1 ====");
    var word = 'Dart';
    var second = 'is';
    var third = 'awesome';
    var fourth = 'and';
    var fifth = 'I';
    var sixth = 'love';
    var seventh = 'it!';

    var words = "${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}";
    print(words);

    print(""); // Pemisah

    // Soal 2
    print("==== Soal Nomor 2 ====");
    var sentence = "I am going to be Flutter Developer";

    var firstWord = sentence[0];
    var secondWord = sentence[2] + sentence[3];
    var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence [8] + sentence[9];
    var fourthWord = sentence[11] + sentence[12];
    var fifthWord = sentence[14] + sentence[15];
    var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence [20] + sentence[21] + sentence[22] + sentence[23];
    var seventhWord = sentence[25] + sentence[26] + sentence[27] + sentence [28] + sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33];

    print('First Word: ' + firstWord);
    print('Second Word: ' + secondWord);
    print('Third Word: ' + thirdWord);
    print('Fourth Word: ' + fourthWord);
    print('Fifth Word: ' + fifthWord);
    print('Sixth Word: ' + sixthWord);
    print('Seventh Word: ' + seventhWord);

    print(""); // Pemisah

    // Soal 3
    print("==== Soal Nomor 3 ====");
    print("Masukan Nama Depan: ");
    String? inputFirstName = stdin.readLineSync();
    print("Masukan Nama Belakang: ");
    String? inputLastName = stdin.readLineSync();

    print("Nama Lengkap Anda Adalah: ${inputFirstName} ${inputLastName}");

    print(""); // Pemisah

    // Soal 4
    print("==== Soal Nomor 4 ====");
    print("Masukan Nilai a: ");
    String? a = stdin.readLineSync();
    print("Masukan Nilai b: ");
    String? b = stdin.readLineSync();
    if(a != null && b != null){
        var inputA = int.parse(a);
        var inputB = int.parse(b);

        var _perkalian = inputA * inputB;
        var _pembagian = inputA / inputB;
        var _penambahan = inputA + inputB;
        var _pengurangan = inputA - inputB;

        print("Perkalian: ${_perkalian}");
        print("Pembagian: ${_pembagian}");
        print("Penambahan: ${_penambahan}");
        print("Pengurangan: ${_pengurangan}");
    }
    
}
