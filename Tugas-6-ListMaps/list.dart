import "dart:io";

void main(){
    // Soal No 1
    print("Masukan angka awal: ");
    var startNum = stdin.readLineSync();
    print("Masukan angka akhir: ");
    var finishNum = stdin.readLineSync();
    if(startNum != null && finishNum != null){
        print(range(int.parse(startNum), int.parse(finishNum)));
    }

    // Soal No 2
    print("Masukan angka awal: ");
    var startNumWithStep = stdin.readLineSync();
    print("Masukan angka akhir: ");
    var finishNumWithStep = stdin.readLineSync();
    print("Masukan step: ");
    var step = stdin.readLineSync();
    if(startNumWithStep != null && finishNumWithStep != null && step != null){
        print(rangeWithStep(int.parse(startNumWithStep), int.parse(finishNumWithStep), int.parse(step)));
    }
    
    // Soal No 3
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ];
    dataHandling(input);

    // Soal No 4
    print("Masukan kata: ");
    var kata = stdin.readLineSync();
    if(kata != null){
        print(balikKata(kata));
    }
}

// Soal No 1
List range(int startNum, int finishNum){
    List<int> list = [];
    if(startNum < finishNum){
        for(var i = startNum; i <= finishNum; i++){
            list.add(i);
        }
    }else{
        for(var i = startNum; i >= finishNum; i--){
            list.add(i);
        }
    }
    return list;
}

// Soal No 2
List rangeWithStep(int startNum, int finishNum, int step){
    List<int> list = [];
    if(startNum < finishNum){
        var i = startNum;
        while(i <= finishNum){
            list.add(i);
            i += step;
        }
    }else{
        var i = startNum;
        while(i >= finishNum){
            list.add(i);
            i -= step;
        }
    }
    return list;
}

// Soal No 3
dataHandling(input){
    for(var i = 0; i < input.length; i++){
        print("Nomor ID: ${input[i][0]}");
        print("Nama Lengkap: ${input[i][1]}");
        print("TTL: ${input[i][2]} ${input[i][3]}");
        print("Hobi: ${input[i][4]}");
        print("");
    }
}

// Soal No 4
balikKata(input){
    List<String> words = [];
    String result = "";
    for(var i = input.length - 1; i >= 0; i--){
        words.add(input[i]);
    }

    for(var i = 0; i < words.length; i++){
        result += words[i];
    }
    return result;
}