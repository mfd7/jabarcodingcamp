import 'dart:math';

class Lingkaran{
    late double _phi;
    late double _radius;

    double get phi => this._phi;

    set phi(double value) => this._phi = value;

    get radius => this._radius;

    set radius( value) => this._radius = value;

    double get luas => _phi * pow(_radius, 2);
}