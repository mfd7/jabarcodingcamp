import 'dart:io';
void main(List<String> args){
    Segitiga segitiga;
    double luasSegitiga;

    segitiga = new Segitiga();
    segitiga._setengah = 0.5;
    print("Masukkan alas: ");
    segitiga._alas = double.parse(stdin.readLineSync()!);
    print("Masukkan tinggi: ");
    segitiga._tinggi = double.parse(stdin.readLineSync()!);
    luasSegitiga = segitiga.hitungLuas();
    print(luasSegitiga);
}

class Segitiga{
    late double _setengah;
    late double _alas;
    late double _tinggi;

    double hitungLuas(){
        return this._setengah * this._alas * this._tinggi;
    }
}