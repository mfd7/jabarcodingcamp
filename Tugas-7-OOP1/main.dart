import 'lingkaran.dart';
import 'dart:io';

void main(List<String> args){
  Lingkaran lingkaran;
  double luasLingkaran;
  var inputRadius;

  lingkaran = new Lingkaran();
  lingkaran.phi = 3.14;
  print("Masukkan radius: ");
  inputRadius = stdin.readLineSync();
  inputRadius = double.parse(inputRadius);
  if(inputRadius < 0.0){
    inputRadius *= -1.0;
  }
  lingkaran.radius = inputRadius!;

  luasLingkaran = lingkaran.luas;
  print(luasLingkaran);
}